library(tidyverse)
library(PairedData)
library(reshape2)

alpha <- 0.05 #TODO: What should alpha be set to do to the correlation matrix??

## import data
NO3_CSV <- '~/Denali/North_Pacific_Data/Denali/DIC2013_Shallow_Data/denali092822_1NO3.csv'

Headers <- read.csv(NO3_CSV, header = FALSE, sep=",", nrows = 1, as.is = TRUE)
Headers[1] = "index"

NO3 <- read.csv(NO3_CSV, header = FALSE, sep=",", stringsAsFactors = TRUE, skip = 2) %>% data.frame()
colnames(NO3) = Headers

HF_CSV <- '~/Denali/North_Pacific_Data/Denali/DIC2013_Shallow_Data/denali_Hf.csv'
HF <- read.csv(HF_CSV, header = FALSE, sep=",", stringsAsFactors = TRUE, skip=2) %>% data.frame()
colnames(HF) = Headers

## Check data import
summary(NO3)
summary(HF)

## Formatting the Dataframes into long format and combine
NO3_long <- NO3 %>% gather(key = analyte, value = value, -index) %>% arrange(index) %>% drop_na(value)
HF_long <- HF %>% gather(key = analyte, value = value, -index) %>% arrange(index) %>% drop_na(value)

df <- inner_join(NO3_long, HF_long, by = c("index" = "index", "analyte" = "analyte"), suffix = c(".NO3", ".HF")) %>%
  rename("NO3" = "value.NO3", "HF" = "value.HF") %>% gather(key = acidification, value = value, NO3, HF) %>%
  mutate(acidification = factor(acidification)) %>%
  mutate(analyte = factor(analyte, levels = c("Sc45(LR)", "Cr52(MR)", "Mn55(MR)", "Fe56(MR)", "Co59(MR)", "Ni61(MR)",
                                              "Cu63(MR)", "Zn66(MR)", "As75(LR)", "Y89(LR)", "Mo98(LR)", "Ag107(LR)",
                                              "Cd111(LR)", "Ba137(LR)", "La139(LR)", "Ce140(LR)", "Pr141(LR)",
                                              "Nd145(LR)", "Sm147(LR)", "Eu153(LR)", "Tb159(LR)", "Gd160(LR)",
                                              "Dy163(LR)", "Ho165(LR)", "Er166(LR)", "Tm169(LR)", "Yb172(LR)",
                                              "Lu175(LR)", "Pb206(LR)", "Pb208(LR)", "U238(LR)"))) %>%
  mutate(index = factor(index)) %>% arrange(index, analyte) %>%
  filter(!grepl('SLRS', index)) # %>% filter(!grepl('blank', index)) ## USE to filter blanks and reference data!

## Paired t-test on one analyte
singleanalyteAnalysis <- function (dataframe){
  #plot paired data for visualization
  plot_HF <- subset(dataframe, acidification == "HF", value)
  plot_NO3 <- subset(dataframe, acidification == "NO3", value)
  p <- plot(paired(plot_HF, plot_NO3), type = "profile")

  #Estimate means
  HF_Summary <- summary(plot_HF$value)
  NO3_Summary <- summary(plot_NO3$value)

  #then need to check the sample size -- looking for 30 samples paired -- or need to test normality
  if (nrow(dataframe) > 60) {
    #Since this is in the normal or big category, use paired t-test.
    result <- t.test(value ~ acidification, data = dataframe, paired = TRUE, var.equal = FALSE)
    return(list("plot" = p, "normality" = NULL, "t.test" = result, "Wilcox.test" = NULL, "HF" = HF_Summary, "NO3" = NO3_Summary))
  } else {
      #to check normality -- Shapiro-Wilk
    differences <- with(dataframe, value[acidification == "HF"] - value[acidification == "NO3"])
    normality <- shapiro.test(differences)

    if (normality$p.value > alpha){
      #Since this is in the normal or big category, use paired t-test.
      result <- t.test(value ~ acidification, data = dataframe, paired = TRUE, var.equal = FALSE)
      return(list("plot" = p, "normality" = normality, "t.test" = result, "Wilcox.test" = NULL, "HF" = HF_Summary, "NO3" = NO3_Summary))
    } else {
      # Since this is not normal or small, use paired wilcox test
      result <- wilcox.test(value ~ acidification, data = dataframe, paired = TRUE)
      return(list("plot" = p, "normality" = normality, "t.test" = NULL, "Wilcox.test" = result, "HF" = HF_Summary, "NO3" = NO3_Summary))
    }
  }
}

#make a new dataframe for blanks
blankDF <- df %>% filter(grepl('blank', index), analyte == "Mn55(MR)")
singleanalyteAnalysis(blankDF)

#make a new dataframe for samples
analyteDF <- df %>% filter(!grepl('blank', index), analyte == "Mn55(MR)")
singleanalyteAnalysis(analyteDF)

## Exploring Co-variance
#correlation -- how good does a trend line fit
#covariance -- correlation amplified by variation; hard to use when data not on the same scale/ magnitude

df_wide <- df %>% filter(!grepl('blank', index)) %>% spread(key = analyte, value = value) #%>%
 # filter(acidification == "HF")
correlation <- cor(df_wide[,3:33])

#visual of the covariance
correlationMelted <- melt(correlation) %>% filter(!(Var1 == Var2))
plot <- ggplot(data = correlationMelted, aes(x = Var1, y = Var2, fill = value)) + geom_tile()
plot

#TODO: Make a loop to go through all analytes

### FIRST ATTEMPT IS CRUDE: WARNING P-HACKS
analytes <- c("Sc45(LR)", "Cr52(MR)", "Mn55(MR)", "Fe56(MR)", "Co59(MR)", "Ni61(MR)", "Cu63(MR)", "Zn66(MR)", "As75(LR)",
           "Y89(LR)", "Mo98(LR)", "Ag107(LR)", "Cd111(LR)", "Ba137(LR)", "La139(LR)", "Ce140(LR)", "Pr141(LR)",
           "Nd145(LR)", "Sm147(LR)", "Eu153(LR)", "Tb159(LR)", "Gd160(LR)", "Dy163(LR)", "Ho165(LR)", "Er166(LR)",
           "Tm169(LR)", "Yb172(LR)", "Lu175(LR)", "Pb206(LR)", "Pb208(LR)", "U238(LR)")

## attempt to reproduce Table 2: Koffman et al.
outputBlanks <- data.frame(analyte = analytes, NO3_mean = double(length(analytes)), HF_mean = double(length(analytes)),
                           MeanofDifferences = double(length(analytes)), p_value = double(length(analytes)),
                           p_lthan_alpha = character(length(analytes)), test = character(length(analytes)))

for (i in 1:nrow(outputBlanks)){
  blankDF <- df %>% filter(grepl('blank', index), analyte == outputBlanks$analyte[i])
  analysis <- singleanalyteAnalysis(blankDF)

  outputBlanks$HF_mean[i] <- analysis$HF["Mean"]
  outputBlanks$NO3_mean[i] <- analysis$NO3["Mean"]
  outputBlanks$MeanofDifferences[i] <- analysis$HF["Mean"] - analysis$NO3["Mean"]

  if (!is.null(analysis$t.test)){
    outputBlanks$p_value[i] <- analysis$t.test$p.value
    outputBlanks$p_lthan_alpha[i] <- ifelse(analysis$t.test$p.value < alpha, "*", "")
    outputBlanks$test[i] <- "t.test"
  }else if (!is.null(analysis$Wilcox.test)){
    outputBlanks$p_value[i] <- analysis$Wilcox.test$p.value
    outputBlanks$p_lthan_alpha[i] <- ifelse(analysis$Wilcox.test$p.value < alpha, "*", "")
    outputBlanks$test[i] <- "Wilcox.test"
  }
}
